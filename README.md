# recipe-app-api-proxy

NGINX proxt app for our recipe app API

## Usage

### Environment variables

* 'LISTEN_PORT' - Port to listen on( Default: '8000')
* 'APP_HOST' - Hostname of the app to forward request to ( default: 'app')
* 'APP_PORT' - Port of the app to forward request to ( default: '9000')
